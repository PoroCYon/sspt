#!/usr/bin/env bash

print_help() {
    >&2 cat <<EOF
Modify the database of installed binaries.
Usage: sspt bin-db -h|-?|--help
           Print this help text and exit.
     - sspt bin-db exists <package-name>
           Check whether the package has been installed. The exit code
           is set to 0 if the package exists, and 1 otherwise.
     - sspt bin-db reg(ister) <package-name> <file containing the list of files the package owns>
           Add the package to the list of registered packages.
           Use \`sspt add\` to actually install the package.
     - sspt bin-db unreg(ister) <package-name>
           Remove the package from the list of registered packages.
NOTE: this subcommand is supposed to be used only by sspt internally.
      Proceed with caution.
EOF
}

main() {
    local ESCAPED_PKG=$(escape_string "$2")

    case "$1" in
        "-h"|"-?"|"--help")
            print_help
            ;;
        "exists")
            if [ $# -lt 2 ]; then   print_help; exit 1; fi
            grep -E -q -e "^$ESCAPED_PKG$" < "$SSPT_BIN_DB_FILE"
            return $?
            ;;
        "reg"|"register")
            if [ $# -lt 3 ]; then   print_help; exit 1; fi
            if ! [ -f "$3" ]; then  print_help; exit 1; fi

            # check whether package exists
            if ! grep -E -q -e "^$ESCAPED_PKG$" < "$SSPT_BIN_DB_FILE"; then
                # doesn't exist
                echo "$2" >> "$SSPT_BIN_DB_FILE"
                cp "$3" "$SSPT_BIN_DB_DIR/$2"
            else
                # update path
                local TMPFILE="$(temp_filename)"
                grep -E -v -e "^$ESCAPED_PKG$" < "$SSPT_BIN_DB_FILE" > "$TMPFILE"
                echo "$2" >> "$TMPFILE"
                mv "$TMPFILE" "$SSPT_BIN_DB_FILE"
                cp "$3" "$SSPT_BIN_DB_DIR/$2"
            fi
            ;;
        "unreg"|"unregister")
            if [ $# -lt 2 ]; then   print_help; exit 1; fi

            if grep -E -q -e "^$ESCAPED_PKG$" < "$SSPT_BIN_DB_FILE"; then
                # exists
                local TMPFILE="$(temp_filename)"
                grep -E -v -e "^$ESCAPED_PKG$" < "$SSPT_BIN_DB_FILE" > "$TMPFILE"
                mv "$TMPFILE" "$SSPT_BIN_DB_FILE"
                rm -f "$SSPT_BIN_DB_DIR/$2"
            else
                >&2 echo "Package doesn't exist."
            fi
            ;;
    esac
}

main "$@"

