#!/usr/bin/env bash

print_help() {
    >&2 cat <<EOF
Purge a package without deleting the installed binaries.
Usage: sspt orphanize [-h|-?|--help]
           Print this help text and exit.
     - sspt orphanize <package-name>
           Orphanize the package.
EOF
}

main() {
    if [ -z "$1" ]; then
        print_help
        exit
    fi

    case "$1" in
        "-h"|"-?"|"--help")
            print_help
            ;;
        *)
            if ! exec_subcommand src-db exists "$1"; then
                >&2 echo "Package '$1' doesn't exist."
                exit 1
            fi

            # 1. unreg in the bin db
            if exec_subcommand bin-db exists "$1"; then
                exec_subcommand bin-db unreg "$1" || exit 1
            else
                >&2 echo "Package '$1' isn't installed."
                exit 1
            fi

            # 2. unregister
            exec_subcommand src-db unreg "$1" || exit 1

            # 3. nuke
            rm -rf "${SSPT_DL_DIR:?}/$1"
            ;;
    esac
}

if [ $# -eq 0 ]; then
    print_help
    exit
fi

main "$@"

