#!/usr/bin/env bash

cwd=$(pwd)

if [ $# -eq 0 ]; then
    MANPATH="$cwd/local/share/man:$MANPATH" \
        LD_LIBRARY_PATH="$cwd/local/lib:$LD_LIBRARY_PATH" \
        PATH="$cwd/local/bin:$PATH" \
        bash
else
    MANPATH="$cwd/local/share/man:$MANPATH" \
        LD_LIBRARY_PATH="$cwd/local/lib:$LD_LIBRARY_PATH" \
        PATH="$cwd/local/bin:$PATH" \
        "$@"
fi

